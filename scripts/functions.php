<?php 


function getStudentMajor($student_id){
	$result = $GLOBALS['db']->query('select 
		aup_um.mid, aup_um.major_name from aup_undergrad_majors aup_um,
		aup_student_academics aup_sa 
		where aup_sa.st_major=aup_um.mid AND aup_sa.st_id=:student_id ')
			 ->bind(':student_id',$student_id)->select();

    return ($result) ;
}
function getStudentMinor($student_id){
	$result = $GLOBALS['db']->query('select 
		aup_um.mid, aup_um.minor_name from aup_undergrad_minor aup_um, 
		aup_student_academics aup_sa
		where aup_sa.st_minor=aup_um.mid AND aup_sa.st_id=:student_id ')
			 ->bind(':student_id',$student_id)->select();
    return ($result);
}

function getStudentStanding($student_id){
	$result = $GLOBALS['db']->query('select aup_ss.name from aup_student_standing aup_ss, aup_student_academics aup_sa
where aup_ss.sid=aup_sa.st_standing and aup_sa.st_id=:student_id ')
			 ->bind(':student_id',$student_id)->select();
    return ($result);
}

function getRequirements($major_id,$requirement_type){
	$result = $GLOBALS['db']->query(

	'SELECT aup_c.*, aup_ah.course_status FROM 
		aup_courses aup_c, 
		aup_undergrad_major_requirement aup_umr, 
		aup_undergrad_majors aup_um, 
		aup_student_academics_history aup_ah
		WHERE 
			aup_c.course_id = aup_umr.major_req_course_id 
			AND aup_ah.course_id = aup_c.course_id 
			AND aup_um.mid = aup_umr.major_id 
			AND aup_umr.requirement_type = :requirement_type 
			AND aup_um.mid = :major_id 
		 ORDER BY aup_ah.course_status ASC'
	)
	->bind(':major_id',$major_id)
	->bind(':requirement_type',$requirement_type)
	->select();
	
	return $result;

}
function getCourseStatusList(){
	$result = $GLOBALS['db']->query(
		'SELECT * FROM aup_student_course_status_list ')->select();
	return $result;
}

function getStatusIDtoColor($id){
	$result = $GLOBALS['db']->query(
		'SELECT status_color FROM aup_student_course_status_list where id=:id ')->bind(':id',$id)->select();
	return $result;	
}
?>
