<?php
/*

PHP/MySQL (PDO)  method with named parameters
---------------------------------------------
https://gist.github.com/luckyshot/9477105

$config = [
  'mysql' => [
    'database' => 'database',
    'user' => 'root',
    'pass' => 'root'
  ],
];

$db = new DB(
	$config['mysql']['user'],
	$config['mysql']['pass'],
	$config['mysql']['database']
);
$dbdebug = new DBdebug();

$select = $db->query("SELECT * FROM items WHERE id = :id")
	->bind(':id', $id)
	->select();

$single = $db->query("SELECT * FROM items WHERE id = :id LIMIT 1")
	->bind(':id', $id)
	->single();

// Returns the inserted ID
$insert = $db->query("INSERT INTO items SET
		aaa = :aaa,
		bbb = :bbb
	")
	->bind(':aaa', $aaa )
	->bind(':bbb', $bbb )
	->insert();

$update = $db->query("UPDATE items SET
		aaa = :aaa
		WHERE id = :id;
	")
	->bind(':aaa', $aaa )
	->bind(':id', $data['id'])
	->execute();

$delete = $db->query("DELETE FROM items
		WHERE id = :id
	  LIMIT 1;
	 ")
	->bind(':id', $data['id'])
	->execute();

*/

class DB {

	private $dbh;
	private $stmt;

	public function __construct($user, $pass, $dbname) {

		try {	
		   $this->dbh = new PDO(
			"mysql:host=localhost;dbname=$dbname",
			$user,
			$pass,
			array( PDO::ATTR_PERSISTENT => true )
			);
			$this->query("SET NAMES 'utf8';");
			$this->execute();
		}catch (PDOException $e) {
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}
  }

	public function query($query) {
	try {	
		$this->stmt = $this->dbh->prepare($query);
		return $this;
			}catch (PDOException $e) {
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}
	
	}

	public function bind($pos, $value, $type = null) {

		if( is_null($type) ) {
			switch( true ) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}

		$this->stmt->bindValue($pos, $value, $type);
		return $this;
	}

	public function execute() {
		try {	
		return $this->stmt->execute();
			}catch (PDOException $e) {
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}

	}

	// Same as execute() but returns ID
	public function insert() {
		$this->stmt->execute();
	}

	public function select( $fetch_style = null ) {
		$this->execute();
		return $this->stmt->fetchAll( PDO::FETCH_ASSOC );
	}

	public function single( $fetch_style = null ) {
		$this->execute();
		return $this->stmt->fetch( PDO::FETCH_ASSOC );
	}
}



/* 
A clone of DB to help in debugging

echo $dbdebug->query("SELECT * FROM items WHERE id = :id")
	->bind(':id', $id)
	->select();
	
*/
class DBdebug {

	private $q;

	public function query( $q )
	{
		$this->q = $q;
		return $this;
	}

	public function bind( $what, $with, $type = null )
	{
		if ( is_int( $with ) )
		{
			$this->q = str_replace( $what, $with, $this->q );
		}
		else
		{
			$this->q = str_replace( $what, "'" . $with . "'", $this->q );
		}
		return $this;
	}

	public function execute() { return $this->q; }
	public function insert() { return $this->q; }
	public function select() { return $this->q; }
	public function single() { return $this->q; }
}
 
 
 $db = new db($username,$password,$db_name);
 $dbdebug  = $dbdebug = new DBdebug();
 $GLOBALS['db'] = $db;
 $GLOBALS['dbdebug'] = $dbdebug;


?>