-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 15, 2016 at 01:11 AM
-- Server version: 10.0.21-MariaDB
-- PHP Version: 5.6.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aup_course_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `aup_courses`
--

CREATE TABLE `aup_courses` (
  `course_id` int(11) NOT NULL,
  `course_number` varchar(200) NOT NULL,
  `course_name` varchar(200) NOT NULL,
  `course_credit` int(11) NOT NULL,
  `course_pre_req` varchar(200) NOT NULL,
  `course_level` int(11) NOT NULL,
  `course_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_courses`
--

INSERT INTO `aup_courses` (`course_id`, `course_number`, `course_name`, `course_credit`, `course_pre_req`, `course_level`, `course_type`) VALUES
(2, 'MA1020', ' APPLIED STATISTICS I ', 4, 'null', 1, 1),
(3, 'CS1040', 'INTRO TO COMPUTER PROGRAMMING I', 5, 'null', 1, 1),
(4, 'CS1050', ' INTRO TO COMPUTER PROGRAMMING II', 5, 'null', 1, 1),
(5, 'CS2071', ' LANGUAGES & DATA STRUCTURES', 4, 'null', 1, 1),
(6, 'CS3048', 'HUMAN-COMPUTER INTERACTION ', 4, 'null', 1, 1),
(7, 'CS3068', 'DATABASE APPLICATIONS ', 4, 'null', 1, 1),
(8, 'CS3051', 'WEB APPLICATIONS', 4, 'null', 1, 1),
(9, 'MA2400', 'DISCRETE MATHEMATICS', 4, 'null', 1, 1),
(10, 'CS3032', 'OPERATING SYSTEMS', 4, 'null', 1, 1),
(11, 'CS3051', 'WEB APPLICATIONS', 4, 'null', 1, 1),
(12, 'CS3053', 'SOFTWARE ENGINEERING', 4, 'null', 1, 1),
(13, 'CS4095', 'SENIOR PROJECT', 1, 'null', 1, 1),
(14, 'EN1010', 'COLLEGE WRITING', 4, 'null', 1, 1),
(15, 'EN2020', 'WRITING & CRITICISM', 4, 'null', 1, 1),
(16, 'FR1200', 'ELEMENTARY FRENCH AND CULTURE II', 4, 'null', 1, 1),
(17, 'FR1100', 'ELEMENTARY FRENCH AND CULTURE I', 4, 'null', 1, 1),
(18, 'CS3026', 'ARTIFICIAL INTELLIGENCE', 4, 'null', 1, 1),
(19, 'AR1010', 'INTRO TO DRAWING', 4, 'null', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `aup_courses_repeat_type`
--

CREATE TABLE `aup_courses_repeat_type` (
  `id` int(11) NOT NULL,
  `repeat_type` varchar(200) NOT NULL,
  `repeat_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_courses_repeat_type`
--

INSERT INTO `aup_courses_repeat_type` (`id`, `repeat_type`, `repeat_desc`) VALUES
(1, 'REGULAR', 'Lorem Ipsum');

-- --------------------------------------------------------

--
-- Table structure for table `aup_programs`
--

CREATE TABLE `aup_programs` (
  `id` int(11) NOT NULL,
  `program_name` varchar(200) NOT NULL,
  `program_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_programs`
--

INSERT INTO `aup_programs` (`id`, `program_name`, `program_desc`) VALUES
(1, 'Undergraduate', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet mi luctus lorem hendrerit pharetra. In hac habitasse platea dictumst. Aenean et porta turpis, ac tempor ex. Morbi convallis condimentum sapien ut pellentesque. Aenean luctus, magna eget volutpat aliquet, nulla augue mattis nunc, non molestie quam sapien ac justo. Proin eu turpis non massa cursus dictum. Ut ullamcorper, enim id interdum condimentum, eros tortor sagittis nisi, ultricies vestibulum metus turpis vel dolor. Phasellus pharetra volutpat leo, id blandit tellus luctus sed. Suspendisse et ex tristique, porttitor mi at, hendrerit leo. In tincidunt nunc a dui aliquam porttitor. Maecenas consectetur sem tellus, vitae dignissim orci molestie sit amet. In dapibus vehicula velit, eu ultrices orci egestas ut. Integer sed nibh sit amet elit dictum varius vel at elit. Nunc vel nisl metus. Mauris sit amet scelerisque augue, eu iaculis diam.\r\n\r\nVestibulum vitae efficitur nibh. In mauris metus, mattis convallis quam nec, efficitur porttitor dui. Mauris nunc nulla, mollis at aliquam nec, eleifend et dolor. Vestibulum velit tellus, volutpat non vehicula at, pulvinar nec sem. Etiam vitae tristique ipsum, sed mollis velit. Aenean vel sem eu lectus ornare cursus. Mauris diam nisi, vehicula quis nulla eu, mollis volutpat purus.\r\n\r\nNullam sed volutpat quam, sed vestibulum elit. Etiam eu lorem eget augue scelerisque rhoncus. Aliquam consequat libero elit, quis tincidunt arcu interdum a. Nam pretium egestas diam dignissim commodo. Sed vel mauris placerat ipsum tempor imperdiet. Nunc at tempus metus. Nam ultrices leo malesuada, viverra turpis eget, convallis justo. Sed suscipit et ante sit amet lobortis. Vivamus mattis odio id sapien tincidunt ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut eu vehicula mi. Quisque tristique massa velit, eu venenatis sem mattis dignissim.\r\n\r\nQuisque euismod magna a dolor lacinia, eget dictum sem dictum. Mauris elementum ipsum eu mi facilisis, ac ultricies nisl molestie. Cras et lacinia erat. Donec pellentesque neque velit, sit amet pretium neque gravida vitae. Nulla facilisi. In rutrum vulputate odio id euismod. Quisque a magna sit amet nunc venenatis cursus. Curabitur mattis gravida mauris quis porta.\r\n\r\nIn non posuere justo, nec porta ipsum. Sed convallis efficitur ligula. Proin ut ullamcorper nulla. Mauris blandit condimentum nisi vel blandit. Proin quis metus non nisl blandit vehicula. Maecenas ullamcorper sit amet ligula vel pulvinar. Aliquam erat volutpat. Aliquam eget enim eu sapien congue convallis sed vel dolor. Aliquam aliquet massa eget mi volutpat, eget lobortis libero euismod. Etiam mollis, lectus non porta tincidunt, purus arcu viverra sapien, at posuere orci magna ac diam. Praesent fringilla, metus eget pulvinar sodales, dui orci condimentum erat, ut accumsan lectus nibh quis mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non luctus metus. Proin non pretium ligula. In fermentum risus in ante malesuada faucibus. Donec nec metus rhoncus, ultricies urna rutrum, aliquet nunc. '),
(2, 'Graduate', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet mi luctus lorem hendrerit pharetra. In hac habitasse platea dictumst. Aenean et porta turpis, ac tempor ex. Morbi convallis condimentum sapien ut pellentesque. Aenean luctus, magna eget volutpat aliquet, nulla augue mattis nunc, non molestie quam sapien ac justo. Proin eu turpis non massa cursus dictum. Ut ullamcorper, enim id interdum condimentum, eros tortor sagittis nisi, ultricies vestibulum metus turpis vel dolor. Phasellus pharetra volutpat leo, id blandit tellus luctus sed. Suspendisse et ex tristique, porttitor mi at, hendrerit leo. In tincidunt nunc a dui aliquam porttitor. Maecenas consectetur sem tellus, vitae dignissim orci molestie sit amet. In dapibus vehicula velit, eu ultrices orci egestas ut. Integer sed nibh sit amet elit dictum varius vel at elit. Nunc vel nisl metus. Mauris sit amet scelerisque augue, eu iaculis diam.\r\n\r\nVestibulum vitae efficitur nibh. In mauris metus, mattis convallis quam nec, efficitur porttitor dui. Mauris nunc nulla, mollis at aliquam nec, eleifend et dolor. Vestibulum velit tellus, volutpat non vehicula at, pulvinar nec sem. Etiam vitae tristique ipsum, sed mollis velit. Aenean vel sem eu lectus ornare cursus. Mauris diam nisi, vehicula quis nulla eu, mollis volutpat purus.\r\n\r\nNullam sed volutpat quam, sed vestibulum elit. Etiam eu lorem eget augue scelerisque rhoncus. Aliquam consequat libero elit, quis tincidunt arcu interdum a. Nam pretium egestas diam dignissim commodo. Sed vel mauris placerat ipsum tempor imperdiet. Nunc at tempus metus. Nam ultrices leo malesuada, viverra turpis eget, convallis justo. Sed suscipit et ante sit amet lobortis. Vivamus mattis odio id sapien tincidunt ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut eu vehicula mi. Quisque tristique massa velit, eu venenatis sem mattis dignissim.\r\n\r\nQuisque euismod magna a dolor lacinia, eget dictum sem dictum. Mauris elementum ipsum eu mi facilisis, ac ultricies nisl molestie. Cras et lacinia erat. Donec pellentesque neque velit, sit amet pretium neque gravida vitae. Nulla facilisi. In rutrum vulputate odio id euismod. Quisque a magna sit amet nunc venenatis cursus. Curabitur mattis gravida mauris quis porta.\r\n\r\nIn non posuere justo, nec porta ipsum. Sed convallis efficitur ligula. Proin ut ullamcorper nulla. Mauris blandit condimentum nisi vel blandit. Proin quis metus non nisl blandit vehicula. Maecenas ullamcorper sit amet ligula vel pulvinar. Aliquam erat volutpat. Aliquam eget enim eu sapien congue convallis sed vel dolor. Aliquam aliquet massa eget mi volutpat, eget lobortis libero euismod. Etiam mollis, lectus non porta tincidunt, purus arcu viverra sapien, at posuere orci magna ac diam. Praesent fringilla, metus eget pulvinar sodales, dui orci condimentum erat, ut accumsan lectus nibh quis mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non luctus metus. Proin non pretium ligula. In fermentum risus in ante malesuada faucibus. Donec nec metus rhoncus, ultricies urna rutrum, aliquet nunc. ');

-- --------------------------------------------------------

--
-- Table structure for table `aup_students_login_info`
--

CREATE TABLE `aup_students_login_info` (
  `id` char(23) NOT NULL,
  `username` varchar(65) NOT NULL DEFAULT '',
  `password` varchar(65) NOT NULL DEFAULT '',
  `email` varchar(65) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `mod_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_students_login_info`
--

INSERT INTO `aup_students_login_info` (`id`, `username`, `password`, `email`, `verified`, `mod_timestamp`) VALUES
('141054567756c0bb7ea76c2', 'a94542', '$2y$10$xm0PTrlWNdKdbJ5iu6uT6eOsXLNUQbKZTQ1s5RZSZLzmg3MqcOWuG', 'a94542@aup.edu', 1, '2016-02-14 17:39:33');

-- --------------------------------------------------------

--
-- Table structure for table `aup_student_academics`
--

CREATE TABLE `aup_student_academics` (
  `st_id` varchar(11) NOT NULL,
  `st_major` int(11) DEFAULT NULL,
  `st_minor` int(11) DEFAULT NULL,
  `st_standing` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_student_academics`
--

INSERT INTO `aup_student_academics` (`st_id`, `st_major`, `st_minor`, `st_standing`) VALUES
('a94542', 1, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `aup_student_standing`
--

CREATE TABLE `aup_student_standing` (
  `sid` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_student_standing`
--

INSERT INTO `aup_student_standing` (`sid`, `name`) VALUES
(1, 'Freshmen'),
(2, 'Sophomore'),
(3, 'Junior'),
(4, 'Senior');

-- --------------------------------------------------------

--
-- Table structure for table `aup_undergrad_majors`
--

CREATE TABLE `aup_undergrad_majors` (
  `mid` int(11) NOT NULL,
  `major_name` varchar(200) NOT NULL,
  `major_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_undergrad_majors`
--

INSERT INTO `aup_undergrad_majors` (`mid`, `major_name`, `major_desc`) VALUES
(1, 'Computer Science', 'Lorem Ipsum');

-- --------------------------------------------------------

--
-- Table structure for table `aup_undergrad_major_requirement`
--

CREATE TABLE `aup_undergrad_major_requirement` (
  `rid` int(11) NOT NULL,
  `major_id` int(11) NOT NULL,
  `major_req_course_id` int(11) NOT NULL,
  `requirement_type` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aup_undergrad_minor`
--

CREATE TABLE `aup_undergrad_minor` (
  `mid` int(11) NOT NULL,
  `minor_name` varchar(200) NOT NULL,
  `minor_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_undergrad_minor`
--

INSERT INTO `aup_undergrad_minor` (`mid`, `minor_name`, `minor_desc`) VALUES
(1, 'Minor in Applied Mathematics', 'Lorem Ipsum');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aup_courses`
--
ALTER TABLE `aup_courses`
  ADD PRIMARY KEY (`course_id`),
  ADD KEY `course_type` (`course_level`),
  ADD KEY `course_type_2` (`course_type`);

--
-- Indexes for table `aup_courses_repeat_type`
--
ALTER TABLE `aup_courses_repeat_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aup_programs`
--
ALTER TABLE `aup_programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aup_students_login_info`
--
ALTER TABLE `aup_students_login_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `password_UNIQUE` (`password`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `aup_student_academics`
--
ALTER TABLE `aup_student_academics`
  ADD PRIMARY KEY (`st_id`),
  ADD KEY `st_major` (`st_major`),
  ADD KEY `st_standing` (`st_standing`),
  ADD KEY `st_minor` (`st_minor`);

--
-- Indexes for table `aup_student_standing`
--
ALTER TABLE `aup_student_standing`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `aup_undergrad_majors`
--
ALTER TABLE `aup_undergrad_majors`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `aup_undergrad_major_requirement`
--
ALTER TABLE `aup_undergrad_major_requirement`
  ADD PRIMARY KEY (`rid`),
  ADD KEY `major_id` (`major_id`,`major_req_course_id`),
  ADD KEY `requirementForMajorID` (`major_req_course_id`);

--
-- Indexes for table `aup_undergrad_minor`
--
ALTER TABLE `aup_undergrad_minor`
  ADD PRIMARY KEY (`mid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aup_courses`
--
ALTER TABLE `aup_courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `aup_courses_repeat_type`
--
ALTER TABLE `aup_courses_repeat_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `aup_programs`
--
ALTER TABLE `aup_programs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `aup_student_standing`
--
ALTER TABLE `aup_student_standing`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `aup_undergrad_majors`
--
ALTER TABLE `aup_undergrad_majors`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `aup_undergrad_major_requirement`
--
ALTER TABLE `aup_undergrad_major_requirement`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aup_undergrad_minor`
--
ALTER TABLE `aup_undergrad_minor`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `aup_courses`
--
ALTER TABLE `aup_courses`
  ADD CONSTRAINT `CourseRepeatType` FOREIGN KEY (`course_type`) REFERENCES `aup_courses_repeat_type` (`id`),
  ADD CONSTRAINT `CourseToProgram` FOREIGN KEY (`course_level`) REFERENCES `aup_programs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aup_student_academics`
--
ALTER TABLE `aup_student_academics`
  ADD CONSTRAINT `studentToMajor` FOREIGN KEY (`st_major`) REFERENCES `aup_undergrad_majors` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `studentToMinor` FOREIGN KEY (`st_minor`) REFERENCES `aup_undergrad_minor` (`mid`),
  ADD CONSTRAINT `studentToStanding` FOREIGN KEY (`st_standing`) REFERENCES `aup_student_standing` (`sid`);

--
-- Constraints for table `aup_undergrad_major_requirement`
--
ALTER TABLE `aup_undergrad_major_requirement`
  ADD CONSTRAINT `coursesRequiredForMajor` FOREIGN KEY (`major_id`) REFERENCES `aup_undergrad_majors` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `requirementForMajorID` FOREIGN KEY (`major_req_course_id`) REFERENCES `aup_courses` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
