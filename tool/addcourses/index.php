<?php
/*  session_start();

  if(isset($_SESSION['username'])){
    header("location:../index.php");
  }*/
include '../../config.php';
include '../../db/db.php';
if(isset($_POST)){

  $query = "INSERT INTO aup_courses (";
  $count = 0;

  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    $query .= $courseInfo ;
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") VALUES (";
  $count = 0;
  $courseName = '';
  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    if($count==4)
      $courseName = $value;
    $query .= "'".trim($value) ."'";
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") ";
  $prepare = $db->query($query);
  $result = $prepare->execute();

  
  if($result){
    echo "New course added!<br>"; 
    echo "Name " . $courseName;
  }
else{echo "error occured";
  
}
}
  


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Add courses</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="../../css/main.css" rel="stylesheet" media="screen">
  </head>

<body>
<div class="container">

     
<form class="form-horizontal" method="POST">
<fieldset>

<!-- Form Name -->
<legend>Add  courses</legend>

<!-- Select Basic -->

<?php 
$requestCourseLevels = $db->query('select id,repeat_type from aup_courses_repeat_type ')->select();
$html = "";
foreach ($requestCourseLevels as $courseLevel ) {
  $html .= '<option value='. $courseLevel['id'] .'>'. $courseLevel['repeat_type'].'</option>';
}
?>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Course Repeat Type</label>
  <div class="col-md-4">
    <select name="course_type"  class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>

<?php 
$requestCourseLevels = $db->query('SELECT   course_id,course_name FROM aup_courses  ')->select();
$html = '<option selected value="null">None</option>';
foreach ($requestCourseLevels as $courseLevel ) {
  $html .= '<option value='. $courseLevel['course_id'] .'>'. $courseLevel['course_name'].'</option>';
}
?>
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Course Pre-Req</label>
  <div class="col-md-4">
    <select name="course_pre_req"  class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Course Number</label>  
  <div class="col-md-4">
  <input name="course_number"  placeholder="i.e CS1040, MA1040 .." class="form-control input-md" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput-course-name">Course Name</label>  
  <div class="col-md-4">
  <input name="course_name" placeholder="Name of the course" class="form-control input-md"  type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Course Credit</label>  
  <div class="col-md-4">
  <input name="course_credit"  placeholder="Number of credits given" class="form-control input-md" type="text">
    
  </div>
</div>
  <div class="form-group">        
      <div class="col-md-6 control-label">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>



</fieldset>
</form>


    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="../../login/js/bootstrap.js"></script>
    <!-- The AJAX login script -->
    <script src="../../login/js/login.js"></script>

  </body>
</html>


