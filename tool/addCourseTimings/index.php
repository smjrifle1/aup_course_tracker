<?php
/*  session_start();

  if(isset($_SESSION['username'])){
    header("location:../index.php");
  }*/
include '../../config.php';
include '../../db/db.php';
if(!empty($_POST)){
  
  $query = "INSERT INTO aup_course_timing (";
  $count = 0;

  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    $query .= $courseInfo ;
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") VALUES (";
  $count = 0;
  $courseName = '';
  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    if($count==4)
      $courseName = $value;
    $query .= "'".trim($value) ."'";
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") ";
  $prepare = $db->query($query);
  $result = $prepare->execute();

  
  if($result){
    echo "New course timing added!<br>"; 
    echo "Name " . $courseName;
  }
else{echo "error occured";}
}
  


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Add course schedule</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="../../css/main.css" rel="stylesheet" media="screen">
  </head>

<body>
<div class="container">

     
<form class="form-horizontal" method="POST">
<fieldset>

<!-- Form Name -->
<legend>Add course schedule</legend>



<?php 
$requestCourseLevels = $db->query('SELECT  course_id,course_name FROM aup_courses  ')->select();
$html = '<option selected value="null">None</option>';
foreach ($requestCourseLevels as $courseLevel ) {
  $html .= '<option value='. $courseLevel['course_id'] .'>'. $courseLevel['course_name'].'</option>';
}
?>
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Select course</label>
  <div class="col-md-4">
    <select name="c_id"  class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Chose start time</label>  
  <div class="col-md-4">
  <input name="c_start"  placeholder="i.e 8 for 8am 18 for 6 (24 hrs) .." class="form-control input-md" type="text">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Chose end time</label>  
  <div class="col-md-4">
  <input name="c_end"  placeholder="i.e 8 for 8am 18 for 6 (24 hrs) .." class="form-control input-md" type="text">
  </div>
</div>
<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Days the classes happen</label>  
  <div class="col-md-4">
  <input name="c_days"  placeholder="M for monday, T for tuesday .." class="form-control input-md" type="text">
  </div>
</div>
<div class="form-group">        
  <div class="col-md-6 control-label">
    <button type="submit" class="btn btn-default">Submit</button>
  </div>
</div>
</fieldset>
</form>


    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="../../login/js/bootstrap.js"></script>
    <!-- The AJAX login script -->
    <script src="../../login/js/login.js"></script>

  </body>
</html>


