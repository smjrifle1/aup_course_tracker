<?php
include '../../config.php';
include '../../db/db.php';

$db = new db($username,$password,$db_name);
$dbdebug  = $dbdebug = new DBdebug();

if(!empty($_POST)){
  $query = "INSERT INTO aup_student_academics_history (";
    // var_dump($_POST);die();
  $count = 0;
  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    $query .= $courseInfo ;
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") VALUES (";
  $count = 0;
  $courseName = '';
  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    if($count==4)
      $courseName = $value;
    $query .= "'".trim($value) ."'";
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") ";
  // die($query)  ;
  $prepare = $db->query($query);
  $result = $prepare->execute();
  


  if($result){
    echo "New status added!<br>"; 
    
  }

  
}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Modify student Courses done list</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="../../css/main.css" rel="stylesheet" media="screen">
  </head>

<body>
<div class="container">

     
<form class="form-horizontal" method="POST">
<fieldset>

<!-- Form Name -->
<legend>Modify student Courses done list</legend>

<!-- Select Basic -->
<?php 
$requestCourses = $db->query('select * from aup_student_academics ')->select();
$html = '<option selected value="null">None</option>';


foreach ($requestCourses as $course ) {
  $html .= '<option value='. $course['st_id'] .'>'. $course['st_id'].'</option>';
}
?>
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Select Student</label>
  <div class="col-md-4">
    <select id="student-selected" name="student_id"  class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Undergrad Courses</label>
  <div class="col-md-4">
    <select name="course_id" id="courses"  disabled class="form-control">
    </select>
  </div>
</div>

<?php 
$requestCourseLevels = $db->query('SELECT * FROM aup_student_course_status_list ')->select();
$html = '<option selected value="null">None</option>';
foreach ($requestCourseLevels as $courseLevel ) {
  $html .= '<option value='. $courseLevel['id'] .'>'. $courseLevel['status_name'].'</option>';
}
?>
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Course status</label>
  <div class="col-md-4">
    <select name="course_status" id="course_status" disabled class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>


<div class="form-group">        
  <div class="col-md-6 control-label">
    <button type="submit" class="btn btn-default">Submit</button>
  </div>
</div>



</fieldset>
</form>


    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="../../login/js/bootstrap.js"></script>
    <!-- The AJAX login script -->
    <script src="../../login/js/login.js"></script>
    <script type="text/javascript">
    $("#student-selected" ).change(function() {
    	var selectvalue = $(this).val();
    	if(selectvalue!=="null"){

  		 $.ajax({url: 'getCourseList.php?s_id='+selectvalue,
             success: function(output) {
                
              $('#courses').html(output);
              
              $('#courses').prop('disabled', false);
              $('#course_status').prop('disabled', false);

              
            },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status + " "+ thrownError);
          }});
    	}
    	else
    		{
    		  $('#courses').prop('disabled', 'disabled');
              $('#courses').html("");

    		  $('#course_status').prop('disabled', 'disabled');
              // $('#courses').html("");
              

    		}
	});
    </script>
  </body>
</html>


