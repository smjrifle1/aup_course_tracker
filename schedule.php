<?php
session_start();
//PUT THIS HEADER ON TOP OF EACH UNIQUE PAGE

if(!isset($_SESSION['username'])){
  header("location:login/main_login.php");
}

include 'config.php';
include 'db/db.php';
include 'scripts/functions.php';

$student_id = $_GET['st_id'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Schedule</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="css/main.css" rel="stylesheet" media="screen">
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css'>

</head>
<body>
  <header>
    <nav class="navbar navbar-default navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">
            <a href="/studentprofile"><img src="image/logo.png" alt="" /></a>
          </a>
          <div class="col-sm-4 logout pull-right">
            <span><?php echo 'Welcome, ' . $_SESSION['username']; ?></span>
            <a href="schedule.php?st_id=<?php echo $_SESSION['username']?>" class="">Schedule</a> 
            <span></span>
            <a href="login/logout.php" class="">Logout</a> 
          </div>
        </div>
      </div>
    </nav>
  </header>
 <div class="container">
 <div class="row">

   <table class="table table-striped" summary="Matrix schedule of courses registered in this term">
        <caption>Schedule for Next semester</caption>
            <thead>
	            <tr>
		            <th>Start Time</th><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        09:00 AM
                    </td>
                    <td></td><td></td><td></td><td>FRENCH AND CULTURE II</td><td></td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        10:35 AM
                                    </td>
                                    <td>FRENCH AND CULTURE II</td><td>FRENCH AND CULTURE II</td><td></td><td>FRENCH AND CULTURE II</td><td>FRENCH AND CULTURE II</td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        12:10 PM
                                    </td>
                                    <td></td><td>APPLIED DIFFERENTIAL EQUATIONS</td><td></td><td></td><td>APPLIED DIFFERENTIAL EQUATIONS</td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        01:45 PM
                                    </td>
                                    <td>HUMAN-COMPUTER INTERACTION</td><td></td><td></td><td>HUMAN-COMPUTER INTERACTION</td><td></td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        04:55 PM
                                    </td>
                                    <td>WEB APPLICATIONS</td><td></td><td></td><td>WEB APPLICATIONS</td><td></td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        06:30 PM
                                    </td>
                                    <td></td><td></td><td>VIDEO JOURNALISM PRACTICUM</td><td></td><td></td>
                                </tr>
                                </tbody></table>
                        <br />
      </div>                  
 </div>

 </body>
 </html>